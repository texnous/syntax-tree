/**
 * @fileoverview Syntax tree structure element tests
 * This file is a part of TeXnous project.
 *
 * @copyright TeXnous project team (http://texnous.org) 2016-2017
 * @license LGPL-3.0
 *
 * This unit test is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This unit test is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this unit
 * test; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA.
 */

"use strict";

const SyntaxTree = require("../../sources/lib/SyntaxTree"); // syntax tree structure elements

let rootToken, token1, token2, token3, token4, token5, token6;

/**
 * Tests of syntax tree structure elements
 * @author Kirill Chuvilin <k.chuvilin@texnous.org>
 */
module.exports = {
	"Token": {
		"constructor": test => {
			test.doesNotThrow(() => token1 = new SyntaxTree.Token({}));
			test.doesNotThrow(() => token3 = new SyntaxTree.Token({}));
			test.doesNotThrow(() => token2 = new SyntaxTree.Token({parentToken: token3}));
			test.throws(() => new SyntaxTree.Token({childTokens: [token2]}));
			test.doesNotThrow(() => token4 = new SyntaxTree.Token({}));
			test.doesNotThrow(() => token5 = new SyntaxTree.Token({}));
			test.doesNotThrow(() => token6 = new SyntaxTree.Token({}));
			test.doesNotThrow(() => rootToken = new SyntaxTree.Token({childTokens: [token1, token5]}));
			test.done();
		},
		"insertChildToken": test => {
			test.doesNotThrow(() => rootToken.insertChildToken(token4, 1));
			test.throws(() => rootToken.insertChildToken(token3, 1));
			test.doesNotThrow(() => rootToken.insertChildToken(token6, 1, 2));
			test.done();
		},
		"insertChildSubtree": test => {
			test.doesNotThrow(() => rootToken.insertChildSubtree(token3, 1));
			test.done();
		},
		"childToken": test => {
			test.equal(rootToken.childToken(0), token1);
			test.equal(token6.childToken(1), token5);
			test.equal(token6.childToken(2), null);
			test.equal(token1.childToken(1), null);
			test.done();
		},
		"childTokens": test => {
			test.deepEqual(rootToken.childTokens, [token1, token3, token6]);
			test.deepEqual(token1.childTokens, [ ]);
			test.deepEqual(token3.childTokens, [token2]);
			test.deepEqual(token6.childTokens, [token4, token5]);
			test.done();
		},
		"toString": test => {
			test.equal(
				rootToken.toString(),
				"SyntaxTree.Token [ SyntaxTree.Token [], SyntaxTree.Token [ SyntaxTree.Token [] ], SyntaxTree.Token [ SyntaxTree.Token [], SyntaxTree.Token [] ] ]"
			);
			test.equal(
				rootToken.toString(undefined, 2),
				"SyntaxTree.Token [ SyntaxTree.Token [], SyntaxTree.Token [ ... ], SyntaxTree.Token [ ... ] ]"
			);
			test.equal(rootToken.toString(true), "SyntaxTree.Token { \"\" }");
			test.equal(rootToken.toString(false), "");
			test.equal(
				rootToken.toString("  "),
				"SyntaxTree.Token [\n" +
				"  SyntaxTree.Token [],\n" +
				"  SyntaxTree.Token [\n" +
				"    SyntaxTree.Token []\n" +
				"  ],\n" +
				"  SyntaxTree.Token [\n" +
				"    SyntaxTree.Token [],\n" +
				"    SyntaxTree.Token []\n" +
				"  ]\n" +
				"]"
			);
			test.equal(
				rootToken.toString("  ", 2),
				"SyntaxTree.Token [\n" +
				"  SyntaxTree.Token [],\n" +
				"  SyntaxTree.Token [ ... ],\n" +
				"  SyntaxTree.Token [ ... ]\n" +
				"]"
			);
			test.done();
		}
	},
	"SyntaxTree": {
		"constructor": test => {
			test.throws(() => new SyntaxTree());
			test.throws(() => new SyntaxTree(rootToken));
			test.doesNotThrow(() => new SyntaxTree("source", rootToken));
			test.done();
		}
	}
};
